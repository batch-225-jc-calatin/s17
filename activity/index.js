/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


	function printWelcomeMessages(){
	let fullName = prompt("Enter your Full Name: ");
	let enterAge = prompt("Enter your Age: ");
	let enterLocation = prompt("Enter your Location: ");

	console.log("My Name:  " + fullName);
	console.log("My Age:  " + enterAge);
	console.log("My Location:  " + enterLocation);
	alert("Hi! Thank You ." );
	
	console.log ("Thank You!");
}
		printWelcomeMessages();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printName() {
	console.log("1. Parokya ni Edgar");
	console.log("2. Callalily");
	console.log("3. Moira");
	console.log("4. RockSteady");
	console.log("5. Itchy Worm");
}
printName();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printMovie() {
	console.log("Rotten Tomatoes rating = 98%");
	console.log("Billionare Codes");

	console.log("Rotten Tomatoes rating = 97%");
	console.log("Matrix");
	
	console.log("Rotten Tomatoes rating = 96%");
	console.log("Steve Jobs");

	console.log("Rotten Tomatoes rating = 95%");
	console.log("hacker");

	console.log("Rotten Tomatoes rating = 94%");
	console.log("Snowden");
}
printMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/






let printFriends = function printUsers(){
	
alert("Hi! Please add the names of your friends." );

	let friend1 = prompt("Enter your first friend's name:" ); 
	let friend2 = prompt("Enter your second friend's name:") ; 
	let friend3 = prompt("Enter your third friend's name:" );




		console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

alert("Hi! Thank You ." );
	
/*
	console.log(friend1);
console.log(friend2);
*/
}
printFriends();
