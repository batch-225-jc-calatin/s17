// Basic function

// [section] function declaration



/*
    The statements and instructions inside a function is not immediately executed when the function is defined.
    They are run/executed when a function is invoked.
    To invoke a declared function, add the name of the function and a parenthesis.

*/


//  Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration




function printName() {
	console.log("My Name is john");
	console.log("My Name is john");
	console.log("My Name is john");
}
printName();


// Function Expression
// not for hoisting 

let variableFunction = function myGreetings(){
console.log("hello world");

}
variableFunction();

// Re- Assigning functions
variableFunction = function myGreetings(){
	console.log("Update");
}
variableFunction();

// Function scoping

// SCope - is the accesibility (visibility) of variables
/*
	a. local/ block scope
	b.global scope
	c. function scope
*/

//Local Variable

{
	let localVar = "Armando Perez";
	console.log(localVar); 
}

let globalVar =" Mr. World";
console.log(globalVar);


//function scope

function showNames(){
	// Funciton scoped variables:
	const functionConst = "John";
	let functionLet = "james";

	console.log(functionConst);
	console.log(functionLet);
}
showNames();

/*console.log(functionConst); // result an error nasa loob kasi yung function 

console.log(functionLet);*/


// Alert() and promt()

// alert () - allows us to show a small window at the top of our browser page to show informartion to our users.

//It, much like alert(), will have the page wait until the user completes or enters their input.

/*alert("Hello User");
function showSampleAlert(){
	alert("Hello user!");
}
showSampleAlert();*/


// ==========================================

// Promt () - allowas us to show a small window at top of the browser to gather user input.

let samplePromt = prompt("ENter your Name: ")
console.log("Hello, " + samplePromt);


function printWelcomeMessages(){
	let firstName = prompt("Enter your First Name: ");
	let lasName = prompt("Enter your Last Name: ");

	console.log("Hello!, " + firstName+ "" + lasName + "!");
	console.log ("Welcome to Mobile Legends");
}
printWelcomeMessages();